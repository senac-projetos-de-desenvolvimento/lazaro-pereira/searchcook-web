import React, { useEffect, useState } from "react";
import Recipe from "../recipe/recipe";
import { Row, Col } from "react-bootstrap";
import api from "../../api";

export default function Listagem() {
  const [recipes, setRecipes] = useState([]);

  const [recipesSearch, setRecipesSearch] = useState([]);

  const searchResult = "Carnes";

  const getRecipes = async () => {
    await api.get("/recipe").then((res) => {
      setRecipes(res.data.response);
      // console.log(res.data.response[1].category);
    });

    // console.log(recipes.length)
    // for(let i=0; i<recipes.length; i++) {
    //   if(recipes[i].category == searchResult) {
    //     setRecipesSearch(recipes[i]);
    //   }
    // }
  };

//   const getRecipes2 = async () => {
//     await api.get("/recipe").then((res) => {
//     for(let i=0; i<(res.data.response).length; i++) {
//       if(res.data.response[i].category === searchResult) {
//         setRecipesSearch(res.data.response[i]);
//       }
//     }
// console.log("opa"+recipesSearch)
//     });
//   };

  //   recipes.forEach( (el) => {
  //      if(el.name === searchResult) {
  //       setRecipesSearch(el);
  //     }
  //   })

  // console.log("opa  " + recipesSearch)

  useEffect(() => {
    getRecipes();
  }, []);

  return (
    <div className="container mt-5">
      <Col md={12}>
        <Row>
          {recipes.map((recipe) => (
            <Recipe
              key={recipe._id}
              id={recipe._id}
              photo={recipe.photo}
              name={recipe.name}
              category={recipe.category}
              prepare={recipe.prepare}
              quantities={recipe.quantities}
            ></Recipe>
          ))}
        </Row>
      </Col>
    </div>
  );
}
