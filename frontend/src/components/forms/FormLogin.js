import React, { useState } from "react";
import {
  Button,
  Form,
  FloatingLabel,
  Container,
  Row,
  Col,
} from "react-bootstrap";
import "./FormLogin.css";
import api from "../../api";

export default function FormLogin({ setToken }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  async function handleLogin() {
    const payload = { email, password };
    console.log("paylod" + payload);
    api
      .post("/user/authenticate", payload)
      .then(({ data }) => {
        localStorage.setItem("token", data.token);
        localStorage.setItem("id", data.user._id);
        localStorage.setItem("name", data.user.name);
        alert("login efetuado com sucesso");
        console.log(
          `token: ${data.token} id: ${data.user._id} nome: ${data.user.name}`
        );
        window.location.href = "/";
      })
      .catch((err) => alert("Usuário ou senha incorretos"));
  }

  return (
    <Container className="">
      <Row>
        <Col md={3}></Col>
        <Col md={6}>
          <Form className="card card-body mt-4 bg-transparent border-0">
            <Form.Group className="mb-3" controlId="formGroupEmail">
              <FloatingLabel
                controlId="floatingInput"
                label="Email address"
                className="mb-3"
              >
                <Form.Control
                  type="email"
                  placeholder="name@example.com"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </FloatingLabel>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formGroupEmail">
              <FloatingLabel controlId="floatingPassword" label="Password">
                <Form.Control
                  type="password"
                  placeholder="Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </FloatingLabel>
            </Form.Group>
          </Form>
          <Button
            className="button-login"
            variant="danger"
            type="submit"
            onClick={() => handleLogin()}
          >
            Entrar
          </Button>
        </Col>
        <Col md={3}></Col>
      </Row>
    </Container>
  );
}
