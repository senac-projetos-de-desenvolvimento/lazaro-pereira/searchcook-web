import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Header from "./components/header/Header";
import FormUser from "./components/forms/FormUser";
import FormRecipe from "./components/forms/FormRecipe";
import FormLogin from "./components/forms/FormLogin";
import Listagem from "./components/listagem/listagem";
import RecipeDetail from "./components/recipe/recipeDetail";
import FormRecipeEdit from "./components/forms/FormRecipeEdit"
import Footer from "./components/footer/footer"

export default function App() {
  return (
    <div>
      <Router>
        <div className="bg-home">
          <Header />
          <Routes>
            <Route path="/" element={<Listagem />} />
            <Route path="/recipe/:id" element={<RecipeDetail />} />
            <Route path="/formrecipe" element={<FormRecipe />} />
            <Route path="/recipe/edit/:id" element={<FormRecipeEdit />} />
            <Route path="/formuser" element={<FormUser />} />
            <Route path="/formlogin" element={<FormLogin />} />
          </Routes>
          <Footer />
        </div>
      </Router>
    </div>
  );
}
