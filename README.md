# SearchCook-web

## Iniciando localmente a aplicação

- Clone o repositório utilizando o git clone

- Dentro do diretório execute o comando `npm install`

- Logo após execute o comando `npm start`

Após executar estes passos o projeto Frontend estará rodando localmente em sua máquina.
